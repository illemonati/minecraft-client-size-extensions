package tech.tioft.minecraft_client_side_extensions.mixin;

import com.mojang.authlib.GameProfile;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.network.message.MessageHandler;
import net.minecraft.network.message.MessageType;
import net.minecraft.network.message.SignedMessage;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import tech.tioft.minecraft_client_side_extensions.MinecraftClientSideExtensions;

import java.util.UUID;

@Mixin(MessageHandler.class)
public class MessageMixin {
    @Inject(at = @At("HEAD"), method = "onChatMessage")
    private void onChatMessage(SignedMessage message, GameProfile sender, MessageType.Parameters params, CallbackInfo ci) {
        MinecraftClient instance = MinecraftClient.getInstance();
        MinecraftClientSideExtensions.LOGGER.info(message.toString());
        if (sender.getId().equals(UUID.fromString(instance.getSession().getUuid()))) {
            MinecraftClientSideExtensions.LOGGER.info("got self message");
            if (message.toString().contains("use item")) {
                MinecraftClientSideExtensions.LOGGER.info("using item");
                try {
                    ((MinecraftClientMixin) instance).invokeDoItemUse();
                } catch (Throwable t) {
                    MinecraftClientSideExtensions.LOGGER.info(t.getMessage());
                }
            }
        }
    }
}
